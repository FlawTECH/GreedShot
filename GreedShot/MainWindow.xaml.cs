﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GreedShot.Model;

namespace GreedShot
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly LinkLoader _loader;

        public MainWindow()
        {
            InitializeComponent();
            _loader = new LinkLoader(this);
        }

        private void InitializeButtonClick(object sender, RoutedEventArgs e)
        {
            ThreadStart starter = _loader.InitializeValues;
            starter += () =>
            {
                _loader.StartChecking(3);
            };
            var initThread = new Thread(starter) {IsBackground = true};
            initThread.Start();
        }

        public void UpdateStatus(string status)
        {
            LabelStatus.Content = "Status: " + status;
        }
    }
}
