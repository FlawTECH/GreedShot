﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using YoloWrapper;

namespace GreedShot.Model
{
    class DarknetClient
    {
        private Darknet _darknet;
        private readonly string _cfgFile;
        private readonly string _weightsFile;
        private readonly string[] _classNames;

        public DarknetClient(string cfgFile, string weightsFile, string namesFile)
        {
            _cfgFile = cfgFile;
            _weightsFile = weightsFile;
            _classNames = File.ReadAllLines(namesFile);
        }

        public void InitializeNetwork()
        {
            _darknet = new Darknet(_cfgFile, _weightsFile);
        }

        public List<NetResult> Detect(Mat image)
        {
            return _darknet.Detect(image);
        }

        public string GetClassForId(uint id)
        {
            return id<_classNames.Length?_classNames[id]:"ID_OUT_OF_BOUNDS";
        }
    }
}
