﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GreedShot.Model
{
    class ImageUtil
    {
        public static byte[] Base64ToByteArray(string image)
        {
            return Convert.FromBase64String(image);
        }

        public static ImageSource ByteArrayToImageSource(byte[] image)
        {
            var bitImage = new BitmapImage();
            var ms = new MemoryStream(image);
            bitImage.BeginInit();
            bitImage.StreamSource = ms;
            bitImage.EndInit();

            return bitImage;
        }

        public static Bitmap ByteArrayToBitmap(byte[] image)
        {
            using (var ms = new MemoryStream(image))
            {
                return new Bitmap(ms);
            }
        }

        public static void SaveImage(Bitmap image, string name)
        {
            using (var memory = new MemoryStream())
            {
                using (var fs = new FileStream(name, FileMode.Create, FileAccess.ReadWrite))
                {
                    image.Save(memory, ImageFormat.Jpeg);
                    var bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
        }
    }
}
