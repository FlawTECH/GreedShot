﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using HtmlAgilityPack;
using Newtonsoft.Json;
using OpenCvSharp;

namespace GreedShot.Model
{
    public class LinkLoader
    {
        private static readonly HttpClient HClient = new HttpClient();
        private readonly MainWindow _window;
        private string _currentCode;
        private DarknetClient _darknet;

        private static readonly Scalar[] Colors = Enumerable.Repeat(false, 10000).Select(x => Scalar.RandomColor())
            .ToArray();

        public delegate void UpdateStatusCallback(string status);

        public LinkLoader(MainWindow window)
        {
            _window = window;
            WebRequest.DefaultWebProxy = null;
        }

        public void InitializeValues()
        {
            HClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0");

            Application.Current.Dispatcher.Invoke(
                new UpdateStatusCallback(_window.UpdateStatus),
                "Uploading dummy ..."
            );

            var getCodeTask = GetNewCode(ImageUtil.Base64ToByteArray(
                "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACklEQVR4nGP6DwABBQECz6AuzQAAAABJRU5ErkJggg=="));
            getCodeTask.Wait();
            _currentCode = SanitizeCode(getCodeTask.Result);
            _currentCode = IncrementCode(_currentCode, 1);

            Application.Current.Dispatcher.Invoke(
                new UpdateStatusCallback(_window.UpdateStatus),
                "Initializing neural network ..."
            );
            _darknet = new DarknetClient("keymodel.cfg", "keymodel.weights", "keymodel.names");
            _darknet.InitializeNetwork();
            Application.Current.Dispatcher.Invoke(
                new UpdateStatusCallback(_window.UpdateStatus),
                "Ready !"
            );
        }

        public void StartChecking(int threads)
        {
            for (var i = 0; i < threads; i++)
            {
                var threadId = i;
                var checkingThread = new Thread(() => CheckImages(IncrementCode(_currentCode, threadId), threads, threadId));
                checkingThread.Start();
            }
        }

        private async void CheckImages(string startCode, int threads, int threadId)
        {
            while (true)
            {
                var directUrl = await GetImageDirectUrl("https://prnt.sc/" + startCode);
                if (!directUrl.StartsWith("//"))
                {
                    var image = await GetImageFromUrl(directUrl);
                    var imageBmp = ImageUtil.ByteArrayToBitmap(image);
                    var imageMat = OpenCvSharp.Extensions.BitmapConverter.ToMat(imageBmp);
                    var detectionResults = _darknet.Detect(imageMat);

                    foreach (var dResult in detectionResults)
                    {
                        var label = $"{_darknet.GetClassForId(dResult.obj_id)} ( {dResult.prob * 100:0.00}%)";
                        var textSize = Cv2.GetTextSize(label, HersheyFonts.HersheyTriplex, 0.5, 1, out var baseline);
                        Cv2.Rectangle(imageMat, new OpenCvSharp.Point(dResult.x, dResult.y), new OpenCvSharp.Point(dResult.x+dResult.w, dResult.y+dResult.h), Colors[dResult.obj_id], 3);
                        Cv2.Rectangle(imageMat, new OpenCvSharp.Point(dResult.x, dResult.y-textSize.Height-baseline), new OpenCvSharp.Point(dResult.x+textSize.Width, dResult.y), Colors[dResult.obj_id], -1);
                        Cv2.PutText(imageMat, label, new OpenCvSharp.Point(dResult.x, dResult.y - baseline), HersheyFonts.HersheyTriplex, 0.5, Scalar.Black);
                        ImageUtil.SaveImage(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(imageMat), "./img/" + directUrl.Split('/')[4]);
                    }
                }

                Console.WriteLine("["+threadId+"] Downloaded "+startCode);
                startCode = IncrementCode(startCode, threads);
            }
        }

        public async Task<string> GetImageDirectUrl(string link)
        {
            var html = await HClient.GetStringAsync(link);
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            return doc.DocumentNode.SelectSingleNode("//meta[@name=\"twitter:image:src\"]").Attributes["content"].Value;
        }

        public async Task<byte[]> GetImageFromUrl(string link)
        {
            return await HClient.GetByteArrayAsync(link);
        }

        public async Task<string> GetNewCode(byte[] image)
        {
            var content = new MultipartFormDataContent("------------------------" + DateTime.Now.Ticks.ToString("x"))
            {
                { new StreamContent(new MemoryStream(image)), "image", "FFFFFF.png" }
            };

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "https://prntscr.com/upload.php");
            requestMessage.Headers.Add("Accept", "application/json, text/javascript, */*; q=0.01");
            requestMessage.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0");
            requestMessage.Content = content;

            using (var message = await HClient.SendAsync(requestMessage))
            {
                var responseBytes = await message.Content.ReadAsByteArrayAsync();
                return JsonConvert.DeserializeObject<PrntscrUploadResponse>(Encoding.ASCII.GetString(responseBytes))
                    .Data;
            }
        }

        public string SanitizeCode(string code)
        {
            return code.Split('/')[3];
        }

        public string IncrementCode(string code, int step)
        {
            var newCode = "";
            var incrementNext = true;
            var firstValue = true;
            for (var i = code.Length - 1; i >= 0; i--)
            {
                char nextChar;
                if (incrementNext)
                {
                    nextChar = (char)(code[i] + (firstValue ? step : 1));
                    if (nextChar > 'z')
                    {
                        var diff = nextChar - 'z';
                        nextChar = (char)('0'+diff-1);
                    }
                    else if (nextChar > '9' && nextChar < 'a')
                    {
                        var diff = nextChar - '9';
                        nextChar = (char)('a'+diff-1);
                        incrementNext = false;
                    }
                    else
                    {
                        incrementNext = false;
                    }
                }
                else
                {
                    nextChar = code[i];
                }

                firstValue = false;
                newCode += nextChar;
            }

            return ReverseString(newCode);
        }

        public string ReverseString(string s)
        {
            var charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
