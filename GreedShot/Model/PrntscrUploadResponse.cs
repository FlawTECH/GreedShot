﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreedShot.Model
{
    public class PrntscrUploadResponse
    {
        public string Status { get; set; }
        public string Data { get; set; }

        public PrntscrUploadResponse(string status, string data)
        {
            Status = status;
            Data = data;
        }
    }
}
